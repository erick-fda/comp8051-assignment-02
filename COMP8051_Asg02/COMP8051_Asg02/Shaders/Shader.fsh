/*========================================================================================
    PhongShader FragmentShader
	
	Performs phong shading.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
 ========================================================================================*/

precision mediump float;

/*========================================================================================
	Uniforms
 ========================================================================================*/
uniform sampler2D texture;
uniform vec4 ambientLightColor;
uniform vec3 diffuseLightPosition;
uniform vec4 diffuseLightColor;
uniform vec3 specularLightPosition;
uniform vec4 specularLightColor;
uniform float specularity;
uniform bool useFlashlight;
uniform bool useFog;
uniform vec4 fogColor;
uniform float fogDensity;

/*========================================================================================
	Varyings
 ========================================================================================*/
varying vec4 vPositionFromCamera;
varying vec3 vNormalFromCamera;
varying vec2 vTexCoordOut;

/*========================================================================================
	Fragment Shader
 ========================================================================================*/
void main()
{
    /* Ambient Light */
    vec4 ambient = ambientLightColor;
    
    /* Diffuse Light */
    vec3 normalVector = normalize(vNormalFromCamera);
    float diffuseLightIntensity = max(0.0, dot(normalVector, normalize(diffuseLightPosition)));
    vec4 diffuse = diffuseLightColor * diffuseLightIntensity;
    
    /* Specular Light */
    vec4 specular = vec4(0.0, 0.0, 0.0, 0.0);
    if (useFlashlight)
    {
        vec3 lineOfSight = normalize(-vPositionFromCamera.xyz);
        vec3 lightVector = normalize(specularLightPosition - vPositionFromCamera.xyz);
        vec3 halfwayVector = normalize(lightVector+lineOfSight);
        float specularLightIntensity = pow(max(dot(normalVector, halfwayVector), 0.0), specularity);
        specular = specularLightIntensity*specularLightColor;
        if( dot(lightVector, normalVector) < 0.0 ) {
            specular = vec4(0.0, 0.0, 0.0, 1.0);
        }
    }
    
    /* Fog */
    float fogDistance;
    float fogFactor;
    
    if (useFog)
    {
        fogDistance = length(vPositionFromCamera);
        fogFactor = 1.0 / (exp(pow((fogDistance * fogDensity), 2.0)));
    }
    
    /* Combine effects of light components and apply to texture. */
    vec4 color = (ambient + diffuse + specular) * texture2D(texture, vTexCoordOut);
    
    /* Apply fog. */
    if (useFog)
    {
        color = mix(fogColor, color, fogFactor);
    }
    
    gl_FragColor = color;
    
    gl_FragColor.a = 1.0;
}
