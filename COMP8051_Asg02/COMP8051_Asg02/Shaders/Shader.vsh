/*========================================================================================
    PhongShader Vertex Shader
 
    Performs phong shading.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
 ========================================================================================*/

precision mediump float;

/*========================================================================================
	Uniforms
 ========================================================================================*/
uniform mat4 modelViewMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

/*========================================================================================
	Attributes
 ========================================================================================*/
attribute vec4 vPosition;
attribute vec3 vNormal;
attribute vec2 vTexCoordIn;

/*========================================================================================
	Varyings
 ========================================================================================*/
varying vec4 vPositionFromCamera;
varying vec3 vNormalFromCamera;
varying vec2 vTexCoordOut;

/*========================================================================================
	Vertex Shader
 ========================================================================================*/
void main()
{
    /* Get vertex position relative to camera. */
    vPositionFromCamera = modelViewMatrix * vPosition;
    
    /* Get normal vector relative to camera. */
    vNormalFromCamera = (normalMatrix * vNormal);
    
    /* Pass through the texture coordinate. */
    vTexCoordOut = vTexCoordIn;
    
    /* Get vertex position relative to world. */
    gl_Position = modelViewProjectionMatrix * vPosition;
}
