/*========================================================================================
    GameObject
	
	Does GameObject things.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
 ========================================================================================*/

/*========================================================================================
	Dependencies
 ========================================================================================*/
#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

/*========================================================================================
	Structs
 ========================================================================================*/
typedef struct
{
    GLfloat *vertices;
    GLfloat *normals;
    GLfloat *texCoords;
    GLuint *indices;
    
    int numVertices;
    int numIndices;
    
    GLuint vertexArray;
    GLuint vertexBuffers[3];
    GLuint indexBuffer;
    
} VertexInfo;

/*========================================================================================
	GameObject
 ========================================================================================*/
/**
	Does GameObject things.
 */
@interface GameObject : NSObject
{
    /*------------------------------------------------------------------------------------
        Instance Variables
     ------------------------------------------------------------------------------------*/
    
}

/*----------------------------------------------------------------------------------------
    Instance Properties
 ----------------------------------------------------------------------------------------*/
@property BOOL enabled;

@property GLKVector3 position;
@property GLKVector3 velocity;
@property GLKVector3 acceleration;

@property GLKVector3 rotation;
@property GLKVector3 rotationVelocity;
@property GLKVector3 rotationAcceleration;

@property GLKVector3 scale;

@property VertexInfo vertexInfo;
@property GLuint texture;

/*----------------------------------------------------------------------------------------
	Instance Methods
 ----------------------------------------------------------------------------------------*/
-(void)update:(float)data;
-(void)destroy;

@end
