//
//  main.m
//  COMP8051_Asg02
//
//  Created by Erick Fernandez de Arteaga on 2017-03-01.
//  Copyright © 2017 Erick Fernandez de Arteaga. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
