/*========================================================================================
    GameViewController
	
	Presents a 3D maze level.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
 
   "Through me the way into the suffering city,
    Though me the way to the eternal pain,
    Through me the way that runs among the lost.
 
    ...
 
    Before me nothing but eternal things
    were made, and I endure eternally.
    Abandon all hope, ye who enter here."
 
    -- Dante Alighieri (The Divine Comedy: Inferno)
	
 ========================================================================================*/

/*========================================================================================
	Dependencies
 ========================================================================================*/
#import "GameViewController.h"
#import <OpenGLES/ES2/glext.h>

#import "GameObject.h"
#import "CrateObject.h"
#import "FloorObject.h"
#import "WallObject.h"

/*========================================================================================
	Macros
 ========================================================================================*/
#define BUFFER_OFFSET(i) ((char *)NULL + (i))

/*========================================================================================
	Enums
 ========================================================================================*/
/* Shader Uniforms */
enum ShaderUniforms
{
    UNIFORM_MODELVIEW_MATRIX,
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    
    UNIFORM_TEXTURE,
    
    UNIFORM_AMBIENT_LIGHT_COLOR,
    UNIFORM_DIFFUSE_LIGHT_POSITION,
    UNIFORM_DIFFUSE_LIGHT_COLOR,
    UNIFORM_SPECULAR_LIGHT_POSITION,
    UNIFORM_SPECULAR_LIGHT_COLOR,
    UNIFORM_SPECULARITY,
    
    UNIFORM_USE_FLASHLIGHT,
    
    UNIFORM_USE_FOG,
    UNIFORM_FOG_COLOR,
    UNIFORM_FOG_DENSITY,
    
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

/* Model Types */
enum FormType
{
    CUBE
};

/*========================================================================================
	GameViewController
 ========================================================================================*/
/**
    Presents a 3D maze level.
 */
@interface GameViewController ()
{
    /*------------------------------------------------------------------------------------
        Instance Fields
     ------------------------------------------------------------------------------------*/
    
}

/*----------------------------------------------------------------------------------------
    Instance Properties
 ----------------------------------------------------------------------------------------*/
@property (strong, nonatomic) EAGLContext *context;

@property (weak, nonatomic) IBOutlet UIImageView *map;
@property (weak, nonatomic) IBOutlet UIImageView *playerSprite;

/*----------------------------------------------------------------------------------------
    Instance Method Declarations
 ----------------------------------------------------------------------------------------*/
- (void)setupGL;
- (void)tearDownGL;
- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;

@end

@implementation GameViewController
{
    /*-------------------------------------------------------------------------------------
        Private Instance Fields
     ------------------------------------------------------------------------------------*/
    /* Screen Ratio */
    GLfloat _screenWidth;
    GLfloat _screenHeight;
    
    /* Shader Values */
    GLuint _shaderProgram;
    
    GLKMatrix4 _projectionMatrix;
    GLKMatrix4 _modelViewMatrix;
    GLKMatrix4 _modelViewProjectionMatrix;
    GLKMatrix3 _normalMatrix;
    
    GLKVector4 _ambientLightColor;
    GLKVector3 _diffuseLightPosition;
    GLKVector4 _diffuseLightColor;
    GLKVector3 _specularLightPosition;
    GLKVector4 _specularLightColor;
    GLfloat _specularity;
    GLboolean _useFlashlight;
    GLboolean _useFog;
    GLKVector4 _fogColor;
    GLfloat _fogDensity;
    
    /* Models */
    VertexInfo _cubeVertexInfo;
    
    /* Textures */
    GLuint _crateTexture;
    GLuint _floorTexture;
    GLuint _wallBothTexture;
    GLuint _wallRightTexture;
    GLuint _wallLeftTexture;
    GLuint _wallNeitherTexture;
    
    /* World Coordinates */
    GLKMatrix4 _worldMatrix;
    GLKVector3 _worldOriginPosition;
    GLKVector3 _worldOriginRotation;
    GLKVector3 _worldOriginScale;
    
    /* Camera Coordinates */
    GLKVector3 _cameraPosition;
    GLKVector3 _cameraRotation;
    GLfloat _cameraZoom;
    
    /* Game Object Management */
    NSMutableArray *_gameObjects;
    NSMutableArray *_gameObjectsToAdd;
    
    /* Other Values */
    GLKVector4 _backgroundColor;
    GLKVector4 _backgroundColorFog;
    GLKVector4 _backgroundColorNoFog;
    BOOL _isDaytime;
    GLKVector4 _dayAmbient;
    GLKVector4 _nightAmbient;
    GLKVector2 _playerSpriteOrigin;
}

/*----------------------------------------------------------------------------------------
	Property Synthesizers
 ----------------------------------------------------------------------------------------*/
@synthesize map;
@synthesize playerSprite;

/*----------------------------------------------------------------------------------------
	Instance Method Implementations
 ----------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------
	
    VIEW SETUP
 
 ----------------------------------------------------------------------------------------*/
/**
    Set up the OpenGL context and initialize variables.
 */
- (void)viewDidLoad
{
    /* Set up for OpenGL drawing. */
    [super viewDidLoad];
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    /* Initialize variables. */
    _worldOriginPosition = GLKVector3Make(0.0f, 0.0f, 0.0f); /* World origin is centered. */
    _worldOriginRotation = GLKVector3Make(0, 0, 0); /* World has no rotation. */
    _worldOriginScale = GLKVector3Make(1, 1, 1); /* World scale is normal. */
    _cameraPosition = GLKVector3Make(0, 0, 0);  /* Camera is centered. */
    _cameraRotation = GLKVector3Make(0, 0, 0);  /* Camera faces forward. */
    _cameraZoom = 90.0f;
    _backgroundColorNoFog = GLKVector4Make(0.3, 0.5, 0.3, 1.0); // Background is green when there is no fog. */
    _isDaytime = NO;    /* Begin with night lighting by default. */
    _dayAmbient = GLKVector4Make(0.9, 0.9, 0.8, 1.0);   /* Daylight is bright and slightly yellow. */
    _nightAmbient = GLKVector4Make(0.4, 0.4, 0.5, 1.0); /* Night light is dark and slightly blue. */
    
    /* Set up view, game, and OpenGL variables. */
    [self getScreenSize];
    [self setUpGame];
    [self setupGL];
}

/**
    Determine the current screen size.
 */
-(void)getScreenSize
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    _screenWidth = screenRect.size.width;
    _screenHeight = screenRect.size.height;
    
}

/*----------------------------------------------------------------------------------------
	
    OPENGL SETUP
 
 ----------------------------------------------------------------------------------------*/
/**
    Initialize OpenGL variables.
    Setup models and textures.
 */
- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
    [self loadShaders];
    
    /* Get locations of shader uniforms. */
    uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(_shaderProgram, "modelViewProjectionMatrix");
    uniforms[UNIFORM_NORMAL_MATRIX] = glGetUniformLocation(_shaderProgram, "normalMatrix");
    uniforms[UNIFORM_MODELVIEW_MATRIX] = glGetUniformLocation(_shaderProgram, "modelViewMatrix");
    uniforms[UNIFORM_TEXTURE] = glGetUniformLocation(_shaderProgram, "texture");
    uniforms[UNIFORM_AMBIENT_LIGHT_COLOR] = glGetUniformLocation(_shaderProgram, "ambientLightColor");
    uniforms[UNIFORM_DIFFUSE_LIGHT_POSITION] = glGetUniformLocation(_shaderProgram, "diffuseLightPosition");
    uniforms[UNIFORM_DIFFUSE_LIGHT_COLOR] = glGetUniformLocation(_shaderProgram, "diffuseLightColor");
    uniforms[UNIFORM_SPECULAR_LIGHT_POSITION] = glGetUniformLocation(_shaderProgram, "specularLightPosition");
    uniforms[UNIFORM_SPECULAR_LIGHT_COLOR] = glGetUniformLocation(_shaderProgram, "specularLightColor");
    uniforms[UNIFORM_SPECULARITY] = glGetUniformLocation(_shaderProgram, "specularity");
    uniforms[UNIFORM_USE_FLASHLIGHT] = glGetUniformLocation(_shaderProgram, "useFlashlight");
    uniforms[UNIFORM_USE_FOG] = glGetUniformLocation(_shaderProgram, "useFog");
    uniforms[UNIFORM_FOG_COLOR] = glGetUniformLocation(_shaderProgram, "fogColor");
    uniforms[UNIFORM_FOG_DENSITY] = glGetUniformLocation(_shaderProgram, "fogDensity");
    
    /* Initialize uniform values. */
    if (_isDaytime)
    {
        _ambientLightColor = _dayAmbient;
    }
    else
    {
        _ambientLightColor = _nightAmbient;
    }
    _diffuseLightPosition = GLKVector3Make(-2.0, 1.0, -2.0);
    _diffuseLightColor = GLKVector4Make(0.3, 0.3, 0.3, 1.0);
    _specularLightPosition = GLKVector3Make(0.0, 0.0, 1.0);
    _specularLightColor = GLKVector4Make(1.0, 1.0, 1.0, 1.0);
    _specularity = 100.0;
    _useFlashlight = YES;   /* Use the flashlight by default. */
    _useFog = YES;  /* Use fog by default. */
    _fogColor = GLKVector4Make(0.5, 0.5, 0.5, 1.0); /* Fog is gray. */
    _fogDensity = 0.3;
    
    _backgroundColorFog = _fogColor;    /* The background color when fog is on is the same as the fog color. */
    if (_useFog)
    {
        _backgroundColor = _backgroundColorFog;
    }
    else
    {
        _backgroundColor = _backgroundColorNoFog;
    }
    
    /* Set OpenGL parameters. */
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glActiveTexture(GL_TEXTURE0);
    
    /* Set up models. */
    [self setUpVertexInfo:&_cubeVertexInfo asFormType:CUBE];
    
    /* Set up textures. */
    _crateTexture = [self setupTexture:@"crate.jpg"];
    _floorTexture = [self setupTexture:@"img_floor_tile.jpg"];
    _wallBothTexture = [self setupTexture:@"img_wall_both.jpg"];
    _wallRightTexture = [self setupTexture:@"img_wall_left.jpg"];
    _wallLeftTexture = [self setupTexture:@"img_wall_right.jpg"];
    _wallNeitherTexture = [self setupTexture:@"img_wall_neither.jpg"];
}

/**
    Set up vertex data for a given model.
 */
-(void)setUpVertexInfo:(VertexInfo*)vertexInfoStruct asFormType:(enum FormType)formType
{
    /* Assign the proper vertex info depending on form type. */
    switch (formType)
    {
        case CUBE:
            vertexInfoStruct->numIndices =
            generateCube(1.0, &vertexInfoStruct->vertices, &vertexInfoStruct->normals, &vertexInfoStruct->texCoords, &vertexInfoStruct->indices, &vertexInfoStruct->numVertices);
            break;
    }
    
    /* Set OpenGL arrays and buffers. */
    glGenVertexArraysOES(1, &vertexInfoStruct->vertexArray);
    glBindVertexArrayOES(vertexInfoStruct->vertexArray);
    
    glGenBuffers(3, vertexInfoStruct->vertexBuffers);
    glGenBuffers(1, &vertexInfoStruct->indexBuffer);
    
    /* Set position attribute. */
    glBindBuffer(GL_ARRAY_BUFFER, vertexInfoStruct->vertexBuffers[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*3*vertexInfoStruct->numVertices, vertexInfoStruct->vertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), BUFFER_OFFSET(0));
    
    /* Set normal attribute. */
    glBindBuffer(GL_ARRAY_BUFFER, vertexInfoStruct->vertexBuffers[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*3*vertexInfoStruct->numVertices, vertexInfoStruct->normals, GL_STATIC_DRAW);
    glEnableVertexAttribArray(GLKVertexAttribNormal);
    glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), BUFFER_OFFSET(0));
    
    /* Set texcoord0 attribute. */
    glBindBuffer(GL_ARRAY_BUFFER, vertexInfoStruct->vertexBuffers[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*3*vertexInfoStruct->numVertices, vertexInfoStruct->texCoords, GL_STATIC_DRAW);
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), BUFFER_OFFSET(0));
    
    /* Set index buffer. */
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexInfoStruct->indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*vertexInfoStruct->numIndices, vertexInfoStruct->indices, GL_STATIC_DRAW);
    
    glBindVertexArrayOES(0);
}

/*----------------------------------------------------------------------------------------
	
    GAME SETUP
 
 ----------------------------------------------------------------------------------------*/
/**
    Set up game objects.
 */
- (void)setUpGame
{
    _playerSpriteOrigin = GLKVector2Make(_screenWidth * 0.46,
                                         _screenHeight * 0.92);
    
    NSLog(@"Starting game...");
    
    NSLog(@"Creating array of game objects to add...");
    _gameObjectsToAdd = [[NSMutableArray alloc] init];
    
    NSLog(@"Creating array of game objects...");
    _gameObjects = [[NSMutableArray alloc]init];
    
    /* Create game objects here. */
    /* Crate */
    CrateObject *crate = [[CrateObject alloc] init];
    crate.rotationVelocity = GLKVector3Make(0, GLKMathDegreesToRadians(30), 0);
    crate.position = GLKVector3Make(0, 0, -5);
    [_gameObjectsToAdd addObject:crate];
    
    /* Floor */
    FloorObject *floor = [[FloorObject alloc] init];
    floor.position = GLKVector3Make(0, -1.2, -13);
    floor.scale = GLKVector3Make(8, 0.3, 8);
    [_gameObjectsToAdd addObject:floor];
    
    /* Outer Front Left Wall */
    WallObject *wall01 = [[WallObject alloc] init];
    wall01.position = GLKVector3Make(-2, 0, -10);
    wall01.scale = GLKVector3Make(1.5, 2, 0.5);
    wall01.wallType = (enum WallType *)WALL_LEFT;
    [_gameObjectsToAdd addObject:wall01];
    
    /* Outer Left Walls */
    WallObject *wall02 = [[WallObject alloc] init];
    wall02.position = GLKVector3Make(-3, 0, -11);
    wall02.rotation = GLKVector3Make(0, -M_PI/2, 0);
    wall02.scale = GLKVector3Make(1.5, 2, 0.5);
    wall02.wallType = (enum WallType *)WALL_RIGHT;
    [_gameObjectsToAdd addObject:wall02];
    
    WallObject *wall03 = [[WallObject alloc] init];
    wall03.position = GLKVector3Make(-3, 0, -12.5);
    wall03.rotation = GLKVector3Make(0, -M_PI/2, 0);
    wall03.scale = GLKVector3Make(1.5, 2, 0.5);
    wall03.wallType = (enum WallType *)WALL_BOTH;
    [_gameObjectsToAdd addObject:wall03];
    
    WallObject *wall04 = [[WallObject alloc] init];
    wall04.position = GLKVector3Make(-3, 0, -14);
    wall04.rotation = GLKVector3Make(0, -M_PI/2, 0);
    wall04.scale = GLKVector3Make(1.5, 2, 0.5);
    wall04.wallType = (enum WallType *)WALL_LEFT;
    [_gameObjectsToAdd addObject:wall04];
    
    /* Outer Back Walls */
    WallObject *wall05 = [[WallObject alloc] init];
    wall05.position = GLKVector3Make(-2, 0, -15);
    wall05.rotation = GLKVector3Make(0, M_PI, 0);
    wall05.scale = GLKVector3Make(1.5, 2, 0.5);
    wall05.wallType = (enum WallType *)WALL_RIGHT;
    [_gameObjectsToAdd addObject:wall05];
    
    WallObject *wall06 = [[WallObject alloc] init];
    wall06.position = GLKVector3Make(0, 0, -15);
    wall06.rotation = GLKVector3Make(0, M_PI, 0);
    wall06.scale = GLKVector3Make(2.5, 2, 0.5);
    wall06.wallType = (enum WallType *)WALL_BOTH;
    [_gameObjectsToAdd addObject:wall06];
    
    WallObject *wall07 = [[WallObject alloc] init];
    wall07.position = GLKVector3Make(2, 0, -15);
    wall07.rotation = GLKVector3Make(0, M_PI, 0);
    wall07.scale = GLKVector3Make(1.5, 2, 0.5);
    wall07.wallType = (enum WallType *)WALL_LEFT;
    [_gameObjectsToAdd addObject:wall07];
    
    /* Outer Right Walls */
    WallObject *wall08 = [[WallObject alloc] init];
    wall08.position = GLKVector3Make(3, 0, -14);
    wall08.rotation = GLKVector3Make(0, M_PI/2, 0);
    wall08.scale = GLKVector3Make(1.5, 2, 0.5);
    wall08.wallType = (enum WallType *)WALL_RIGHT;
    [_gameObjectsToAdd addObject:wall08];
    
    WallObject *wall09 = [[WallObject alloc] init];
    wall09.position = GLKVector3Make(3, 0, -12.5);
    wall09.rotation = GLKVector3Make(0, M_PI/2, 0);
    wall09.scale = GLKVector3Make(1.5, 2, 0.5);
    wall09.wallType = (enum WallType *)WALL_BOTH;
    [_gameObjectsToAdd addObject:wall09];
    
    WallObject *wall10 = [[WallObject alloc] init];
    wall10.position = GLKVector3Make(3, 0, -11);
    wall10.rotation = GLKVector3Make(0, M_PI/2, 0);
    wall10.scale = GLKVector3Make(1.5, 2, 0.5);
    wall10.wallType = (enum WallType *)WALL_LEFT;
    [_gameObjectsToAdd addObject:wall10];
    
    /* Outer Front Right Wall */
    WallObject *wall11 = [[WallObject alloc] init];
    wall11.position = GLKVector3Make(2, 0, -10);
    wall11.scale = GLKVector3Make(1.5, 2, 0.5);
    wall11.wallType = (enum WallType *)WALL_RIGHT;
    [_gameObjectsToAdd addObject:wall11];
    
    /* Inner Front Wall */
    WallObject *wall12 = [[WallObject alloc] init];
    wall12.position = GLKVector3Make(0, 0, -11.5);
    wall12.scale = GLKVector3Make(2.5, 2, 0.5);
    wall12.wallType = (enum WallType *)WALL_BOTH;
    [_gameObjectsToAdd addObject:wall12];
    
    /* Inner Left Wall */
    WallObject *wall13 = [[WallObject alloc] init];
    wall13.position = GLKVector3Make(-1, 0, -12.1);
    wall13.rotation = GLKVector3Make(0, M_PI/2, 0);
    wall13.scale = GLKVector3Make(1.5, 2, 0.5);
    wall13.wallType = (enum WallType *)WALL_BOTH;
    [_gameObjectsToAdd addObject:wall13];
    
    /* Inner Right Wall */
    WallObject *wall14 = [[WallObject alloc] init];
    wall14.position = GLKVector3Make(1, 0, -12.1);
    wall14.rotation = GLKVector3Make(0, -M_PI/2, 0);
    wall14.scale = GLKVector3Make(1.5, 2, 0.5);
    wall14.wallType = (enum WallType *)WALL_BOTH;
    [_gameObjectsToAdd addObject:wall14];
    
    /* Inner Room Front Wall */
    WallObject *wall15 = [[WallObject alloc] init];
    wall15.position = GLKVector3Make(0, 0, -11.6);
    wall15.rotation = GLKVector3Make(0, M_PI, M_PI);
    wall15.scale = GLKVector3Make(1.5, 2, 0.5);
    wall15.wallType = (enum WallType *)WALL_NEITHER;
    [_gameObjectsToAdd addObject:wall15];
    
    /* Inner Room Left Wall */
    WallObject *wall16 = [[WallObject alloc] init];
    wall16.position = GLKVector3Make(-0.95, 0, -12.1);
    wall16.rotation = GLKVector3Make(0, -M_PI/2, 0);
    wall16.scale = GLKVector3Make(1.45, 2, 0.5);
    wall16.wallType = (enum WallType *)WALL_RIGHT;
    [_gameObjectsToAdd addObject:wall16];
    
    /* Inner Room Right Wall */
    WallObject *wall17 = [[WallObject alloc] init];
    wall17.position = GLKVector3Make(0.95, 0, -12.1);
    wall17.rotation = GLKVector3Make(0, M_PI/2, 0);
    wall17.scale = GLKVector3Make(1.45, 2, 0.5);
    wall17.wallType = (enum WallType *)WALL_LEFT;
    [_gameObjectsToAdd addObject:wall17];
    
    NSLog(@"Done setting up game.");
}

/*----------------------------------------------------------------------------------------
	
    UPDATE AND RENDER
 
 ----------------------------------------------------------------------------------------*/
/**
    Update game objects.
 */
- (void)update
{
    /* Remove game objects that have been disabled. */
    for(NSInteger i = _gameObjects.count - 1; i >= 0; i--)
    {
        GameObject *gameObject = _gameObjects[i];
        if(!gameObject.enabled)
        {
            [_gameObjects removeObjectAtIndex:i];
        }
    }
    
    /* Add game objects that have been registered to add to the scene. */
    for(id gameObject in _gameObjectsToAdd)
    {
        [_gameObjects addObject:gameObject];
        [self setModelForGameObject:gameObject];
    }
    [_gameObjectsToAdd removeAllObjects];
    
    /* Update each game object. */
    for(id thisObject in _gameObjects)
    {
        GameObject *gameObject = (GameObject*)thisObject;
        [gameObject update:self.timeSinceLastUpdate];
    }
}

/**
    Draw the scene.
 */
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    /* Clear the background and buffers. */
    glClearColor(_backgroundColor.x, _backgroundColor.y, _backgroundColor.z, _backgroundColor.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    /* Place the camera. */
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
    _projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(_cameraZoom), aspect, 0.1f, 100.0f);
    _projectionMatrix = GLKMatrix4Translate(_projectionMatrix, _cameraPosition.x, _cameraPosition.y, _cameraPosition.z);
    _projectionMatrix = GLKMatrix4Rotate(_projectionMatrix, _cameraRotation.x, 1, 0, 0);
    _projectionMatrix = GLKMatrix4Rotate(_projectionMatrix, _cameraRotation.y, 0, 1, 0);
    _projectionMatrix = GLKMatrix4Rotate(_projectionMatrix, _cameraRotation.z, 0, 0, 1);
    
    /* Set up the world origin. */
    _worldMatrix = GLKMatrix4MakeTranslation(_worldOriginPosition.x, _worldOriginPosition.y, _worldOriginPosition.z);
    _worldMatrix = GLKMatrix4Rotate(_worldMatrix, _worldOriginRotation.x, 1, 0, 0);
    _worldMatrix = GLKMatrix4Rotate(_worldMatrix, _worldOriginRotation.y, 0, 1, 0);
    _worldMatrix = GLKMatrix4Rotate(_worldMatrix, _worldOriginRotation.z, 0, 0, 1);
    _worldMatrix = GLKMatrix4Scale(_worldMatrix, _worldOriginScale.x, _worldOriginScale.y, _worldOriginScale.z);
    
    /* Render each game object. */
    for(id gameObject in _gameObjects)
    {
        [self renderObject:(GameObject*)gameObject];
    }

}

/**
    Set which model and texture a game object will use.
 */
-(void)setModelForGameObject:(GameObject*)object
{
    /* Set the GameObject's model based on its class. */
    if ([object isKindOfClass:[CrateObject class]])
    {
        object.vertexInfo = _cubeVertexInfo;
        object.texture = _crateTexture;
    }
    else if ([object isKindOfClass:[FloorObject class]])
    {
        object.vertexInfo = _cubeVertexInfo;
        object.texture = _floorTexture;
    }
    else if ([object isKindOfClass:[WallObject class]])
    {
        object.vertexInfo = _cubeVertexInfo;
        
        WallObject *wall = (WallObject *)object;
        switch ((int)wall.wallType)
        {
            case WALL_BOTH:
                wall.texture = _wallBothTexture;
                break;
            case WALL_RIGHT:
                wall.texture = _wallRightTexture;
                break;
            case WALL_LEFT:
                wall.texture = _wallLeftTexture;
                break;
            case WALL_NEITHER:
                wall.texture = _wallNeitherTexture;
                break;
        }
    }
}

/**
    Render the given GameObject.
 */
-(void)renderObject:(GameObject*)gameObject
{
    /* Translate the object. */
    _modelViewMatrix = _worldMatrix;
    _modelViewMatrix = GLKMatrix4Translate(_modelViewMatrix, gameObject.position.x, gameObject.position.y, gameObject.position.z);
    
    /* Rotate the object. */
    _modelViewMatrix = GLKMatrix4Rotate(_modelViewMatrix, gameObject.rotation.x, 1, 0, 0);
    _modelViewMatrix = GLKMatrix4Rotate(_modelViewMatrix, gameObject.rotation.y, 0, 1, 0);
    _modelViewMatrix = GLKMatrix4Rotate(_modelViewMatrix, gameObject.rotation.z, 0, 0, 1);
    
    /* Scale the object. */
    _modelViewMatrix = GLKMatrix4Scale(_modelViewMatrix, gameObject.scale.x, gameObject.scale.y, gameObject.scale.z);
    
    /* Make the normal and MVP matrices. */
    _modelViewMatrix = GLKMatrix4Multiply(_worldMatrix, _modelViewMatrix);
    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(_modelViewMatrix), NULL);
    _modelViewProjectionMatrix = GLKMatrix4Multiply(_projectionMatrix, _modelViewMatrix);
    
    /* Set uniform values. */
    glBindVertexArrayOES(gameObject.vertexInfo.vertexArray);
    glUseProgram(_shaderProgram);
    
    glBindTexture(GL_TEXTURE_2D, gameObject.texture);
    glUniform1i(uniforms[UNIFORM_TEXTURE], 0);
    
    glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEW_MATRIX], 1, 0, _modelViewMatrix.m);
    glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, _modelViewProjectionMatrix.m);
    glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
    glUniform4fv(uniforms[UNIFORM_AMBIENT_LIGHT_COLOR], 1, _ambientLightColor.v);
    glUniform3fv(uniforms[UNIFORM_DIFFUSE_LIGHT_POSITION], 1, _diffuseLightPosition.v);
    glUniform4fv(uniforms[UNIFORM_DIFFUSE_LIGHT_COLOR], 1, _diffuseLightColor.v);
    glUniform3fv(uniforms[UNIFORM_SPECULAR_LIGHT_POSITION], 1, _specularLightPosition.v);
    glUniform4fv(uniforms[UNIFORM_SPECULAR_LIGHT_COLOR], 1, _specularLightColor.v);
    glUniform1f(uniforms[UNIFORM_SPECULARITY], _specularity);
    glUniform1i(uniforms[UNIFORM_USE_FLASHLIGHT], _useFlashlight);
    glUniform1i(uniforms[UNIFORM_USE_FOG], _useFog);
    glUniform4fv(uniforms[UNIFORM_FOG_COLOR], 1, _fogColor.v);
    glUniform1f(uniforms[UNIFORM_FOG_DENSITY], _fogDensity);
    
    /* Draw the object. */
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gameObject.vertexInfo.indexBuffer);
    glDrawElements(GL_TRIANGLES, gameObject.vertexInfo.numIndices, GL_UNSIGNED_INT, 0);
}

/*----------------------------------------------------------------------------------------
	
    GESTURE RESPONDERS
 
 ----------------------------------------------------------------------------------------*/
/**
    Move the player on a one-finger pan.
 */
- (IBAction)onPan:(UIPanGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged)
    {
        CGPoint drag = [sender translationInView:self.view];
        
        /* If it's a one-finger pan, move the player. */
        if (sender.numberOfTouches == 1)
        {
            _worldOriginPosition.x += -drag.x * 0.01;
            _worldOriginPosition.z += -drag.y * 0.01;
            
            /* Move the player sprite on the map. */
            [playerSprite setFrame:CGRectMake(playerSprite.frame.origin.x + (drag.x * (_screenWidth * 0.0021)),
                                              playerSprite.frame.origin.y + (drag.y * (_screenHeight * 0.0011)),
                                              playerSprite.frame.size.width,
                                              playerSprite.frame.size.height)];
        }
//        /* If it's a two-finger pan, turn the player. */
//        else if (sender.numberOfTouches == 2)
//        {
//            _cameraRotation.y += drag.x * 0.01;
//        }
    }
    
    /* Reset the translation (effectively turns the translation member into a delta). */
    [sender setTranslation:CGPointZero inView:self.view];
}

/**
    Reset the player's position on a one-finger double-tap.
 */
- (IBAction)onOneFingerDoubleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateRecognized)
    {
        _worldOriginPosition = GLKVector3Make(0, 0, 0);
        _cameraRotation = GLKVector3Make(0, 0, 0);
        
        /* Reset the player sprite on the map. */
        [playerSprite setFrame:CGRectMake(_playerSpriteOrigin.x,
                                          _playerSpriteOrigin.y,
                                          playerSprite.frame.size.width,
                                          playerSprite.frame.size.height)];
    }
}

/**
    Show and hide the map on a two-finger double-tap.
 */
- (IBAction)onTwoFingerDoubleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateRecognized)
    {
        map.hidden = !map.hidden;
        playerSprite.hidden = !playerSprite.hidden;
    }
}


/**
    Toggle day/night lighting when the day/night button is pressed.
 */
- (IBAction)onDayNightButton:(UIButton *)sender
{
    _isDaytime = !_isDaytime;
    
    if (_isDaytime)
    {
        _ambientLightColor = _dayAmbient;
    }
    else
    {
        _ambientLightColor = _nightAmbient;
    }
}

/* Toggle for when the fog button is pressed. */
- (IBAction)onToggleFogButton:(UIButton *)sender
{
    _useFog = !_useFog;
    
    if (_useFog)
    {
        _backgroundColor = _backgroundColorFog;
    }
    else
    {
        _backgroundColor = _backgroundColorNoFog;
    }
}


/**
    Toggles the flashlight when the flashlight button is pressed.
 */
- (IBAction)onFlashlightButton:(UIButton *)sender
{
    _useFlashlight = !_useFlashlight;
}

/*----------------------------------------------------------------------------------------
	
    OPENGL HELPER FUNCTIONS
 
 ----------------------------------------------------------------------------------------*/
/**
    Generate vertices, normals, texture coordinates and indices for cube.
    Adapted from Dan Ginsburg, Budirijanto Purnomo from the book 
    OpenGL(R) ES 2.0 Programming Guide.
 */
int generateCube(float scale, GLfloat **vertices, GLfloat **normals,
                 GLfloat **texCoords, GLuint **indices, int *numVerts)
{
    int i;
    int numVertices = 24;
    int numIndices = 36;
    
    GLfloat cubeVerts[] =
    {
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f,  0.5f,
        0.5f, -0.5f,  0.5f,
        0.5f, -0.5f, -0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f,  0.5f,  0.5f,
        0.5f,  0.5f,  0.5f,
        0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f,  0.5f, -0.5f,
        0.5f,  0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, 0.5f,
        -0.5f,  0.5f, 0.5f,
        0.5f,  0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        0.5f, -0.5f,  0.5f,
        0.5f,  0.5f,  0.5f,
        0.5f,  0.5f, -0.5f,
    };
    
    GLfloat cubeNormals[] =
    {
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
    };
    
    GLfloat cubeTex[] =
    {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
    };
    
    // Allocate memory for buffers
    if ( vertices != NULL )
    {
        *vertices = malloc ( sizeof ( GLfloat ) * 3 * numVertices );
        memcpy ( *vertices, cubeVerts, sizeof ( cubeVerts ) );
        
        for ( i = 0; i < numVertices * 3; i++ )
        {
            ( *vertices ) [i] *= scale;
        }
    }
    
    if ( normals != NULL )
    {
        *normals = malloc ( sizeof ( GLfloat ) * 3 * numVertices );
        memcpy ( *normals, cubeNormals, sizeof ( cubeNormals ) );
    }
    
    if ( texCoords != NULL )
    {
        *texCoords = malloc ( sizeof ( GLfloat ) * 2 * numVertices );
        memcpy ( *texCoords, cubeTex, sizeof ( cubeTex ) ) ;
    }
    
    
    // Generate the indices
    if ( indices != NULL )
    {
        GLuint cubeIndices[] =
        {
            0, 2, 1,
            0, 3, 2,
            4, 5, 6,
            4, 6, 7,
            8, 9, 10,
            8, 10, 11,
            12, 15, 14,
            12, 14, 13,
            16, 17, 18,
            16, 18, 19,
            20, 23, 22,
            20, 22, 21
        };
        
        *indices = malloc ( sizeof ( GLuint ) * numIndices );
        memcpy ( *indices, cubeIndices, sizeof ( cubeIndices ) );
    }
    
    if (numVerts != NULL)
        *numVerts = numVertices;
    return numIndices;
}

/**
    Load in and set up texture image.
    
    Adapted from Ray Wenderlich, the Virgil to our Dante on the hellish 
    journey through the nine circles of OpenGL.
 */
- (GLuint)setupTexture:(NSString *)fileName
{
    /* Set up a CGImageRef for the texture. */
    CGImageRef spriteImage = [UIImage imageNamed:fileName].CGImage;
    if (!spriteImage) {
        NSLog(@"Failed to load image %@", fileName);
        exit(1);
    }
    
    /* Get height and width. */
    size_t width = CGImageGetWidth(spriteImage);
    size_t height = CGImageGetHeight(spriteImage);
    
    /* Get image as bytes. */
    GLubyte *spriteData = (GLubyte *) calloc(width*height*4, sizeof(GLubyte));
    
    /* Make a context to hold the image. */
    CGContextRef spriteContext = CGBitmapContextCreate(spriteData, width, height, 8, width*4, CGImageGetColorSpace(spriteImage), kCGImageAlphaPremultipliedLast);
    
    /* Draw the image into the context. */
    CGContextDrawImage(spriteContext, CGRectMake(0, 0, width, height), spriteImage);
    
    /* Release the context. */
    CGContextRelease(spriteContext);
    
    /* Generate and bind the texture. */
    GLuint texName;
    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);
    
    /* Set texture parameters. */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    /* Fill the testure with the image data. */
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, spriteData);
    
    /* Free the image bytes and return the address of the texture */
    free(spriteData);
    return texName;
}

/**
    Load and compile shaders at runtime.
 */
- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    /* Create the shader program. */
    _shaderProgram = glCreateProgram();
    
    /* Prepare the vertex shader. */
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    /* Prepare the fragment shader. */
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    /* Attach shaders to program. */
    glAttachShader(_shaderProgram, vertShader);
    glAttachShader(_shaderProgram, fragShader);
    
    /* Bind attribute location. */
    glBindAttribLocation(_shaderProgram, GLKVertexAttribPosition, "vPosition");
    glBindAttribLocation(_shaderProgram, GLKVertexAttribNormal, "vNormal");
    glBindAttribLocation(_shaderProgram, GLKVertexAttribTexCoord0, "vTexCoordIn");
    
    /* Link the program. */
    if (![self linkProgram:_shaderProgram]) {
        NSLog(@"Failed to link program: %d", _shaderProgram);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_shaderProgram) {
            glDeleteProgram(_shaderProgram);
            _shaderProgram = 0;
        }
        
        return NO;
    }
    
    /* Release shaders. */
    if (vertShader) {
        glDetachShader(_shaderProgram, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_shaderProgram, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

/**
    Compile the given shader.
 */
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

/**
    Link the given shader program.
 */
- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

/**
    Validate the given shader program.
 */
- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

/*----------------------------------------------------------------------------------------
	
    TEARDOWN
 
 ----------------------------------------------------------------------------------------*/
/**
    Deallocate memory on teardown.
 */
- (void)dealloc
{
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

/**
    Free up what memory is possible when a memory warning is received.
 */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }
}

/**
    Delete OpenGL variables.
 */
- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    if (_shaderProgram) {
        glDeleteProgram(_shaderProgram);
        _shaderProgram = 0;
    }
}

/**
    Remove all references to game objects.
 */
-(void)tearDownGame
{
    [_gameObjects removeAllObjects];
    _gameObjects = nil;
}

@end
