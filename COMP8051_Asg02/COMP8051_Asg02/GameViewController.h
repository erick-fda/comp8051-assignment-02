/*========================================================================================
    GameViewController
	
	Presents a 3D maze level.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
 
   "Through me the way into the suffering city,
    Though me the way to the eternal pain,
    Through me the way that runs among the lost.
 
    ...
 
    Before me nothing but eternal things
    were made, and I endure eternally.
    Abandon all hope, ye who enter here."
 
    -- Dante Alighieri (The Divine Comedy: Inferno)
	
 ========================================================================================*/

/*========================================================================================
	Dependencies
 ========================================================================================*/
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

/*========================================================================================
	GameViewController
 ========================================================================================*/
/**
	Does GameViewController things.
 */
@interface GameViewController : GLKViewController
{
    /*------------------------------------------------------------------------------------
        Instance Variables
     ------------------------------------------------------------------------------------*/
    
}

/*----------------------------------------------------------------------------------------
    Instance Properties
 ----------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------
	Instance Methods
 ----------------------------------------------------------------------------------------*/


@end
