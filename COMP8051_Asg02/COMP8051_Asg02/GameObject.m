/*========================================================================================
    GameObject
	
	Does GameObject things.
	
	@author Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	@version 0.1.0
	@file
	
 ========================================================================================*/

/*========================================================================================
	Dependencies
 ========================================================================================*/
#import "GameObject.h"

/*========================================================================================
	GameObject
 ========================================================================================*/
/**
    Does GameObject things.
 */
@interface GameObject()
{
    /*------------------------------------------------------------------------------------
        Instance Variables
     ------------------------------------------------------------------------------------*/
    
}

/*----------------------------------------------------------------------------------------
    Instance Properties
 ----------------------------------------------------------------------------------------*/


@end

@implementation GameObject

/*----------------------------------------------------------------------------------------
    Property Synthesizers
 ----------------------------------------------------------------------------------------*/
@synthesize enabled;
@synthesize position;
@synthesize velocity;
@synthesize acceleration;
@synthesize rotation;
@synthesize rotationVelocity;
@synthesize rotationAcceleration;
@synthesize scale;
@synthesize vertexInfo;
@synthesize texture;

/*----------------------------------------------------------------------------------------
	Instance Methods
 ----------------------------------------------------------------------------------------*/
/**
    Initialize variables.
 */
-(id)init
{
    self = [super init];
    
    if (self)
    {
        enabled = true;
        position = GLKVector3Make(0.0, 0.0, 0.0);
        velocity = GLKVector3Make(0.0, 0.0, 0.0);
        acceleration = GLKVector3Make(0.0, 0.0, 0.0);
        rotation = GLKVector3Make(0.0, 0.0, 0.0);
        rotationVelocity = GLKVector3Make(0.0, 0.0, 0.0);
        rotationAcceleration = GLKVector3Make(0.0, 0.0, 0.0);
        scale = GLKVector3Make(1.0, 1.0, 1.0);
    }
    
    return self;
}

/**
    Update object values.
 */
-(void)update:(float)deltaTime
{
    GLKVector3 newPosition;
    newPosition.x = self.position.x + (deltaTime * self.velocity.x);
    newPosition.y = self.position.y + (deltaTime * self.velocity.y);
    newPosition.z = self.position.z + (deltaTime * self.velocity.z);
    self.position = newPosition;
    
    GLKVector3 newVelocity;
    newVelocity.x = self.velocity.x + (deltaTime * self.acceleration.x);
    newVelocity.y = self.velocity.y + (deltaTime * self.acceleration.y);
    newVelocity.z = self.velocity.z + (deltaTime * self.acceleration.z);
    self.velocity = newVelocity;
    
    GLKVector3 newRotation;
    newRotation.x = self.rotation.x + (deltaTime * self.rotationVelocity.x);
    newRotation.y = self.rotation.y + (deltaTime * self.rotationVelocity.y);
    newRotation.z = self.rotation.z + (deltaTime * self.rotationVelocity.z);
    self.rotation = newRotation;
    
    GLKVector3 newRotationVelocity;
    newRotationVelocity.x = self.rotationVelocity.x + (deltaTime * self.rotationAcceleration.x);
    newRotationVelocity.y = self.rotationVelocity.y + (deltaTime * self.rotationAcceleration.y);
    newRotationVelocity.z = self.rotationVelocity.z + (deltaTime * self.rotationAcceleration.z);
    self.rotationVelocity = newRotationVelocity;
}

/**
    Set the game object to be destroyed on the next frame.
 */
-(void)destroy
{
    enabled = NO;
}

@end
