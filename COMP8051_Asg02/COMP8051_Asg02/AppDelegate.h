//
//  AppDelegate.h
//  COMP8051_Asg02
//
//  Created by Erick Fernandez de Arteaga on 2017-03-01.
//  Copyright © 2017 Erick Fernandez de Arteaga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

