/*========================================================================================
	COMP8051 Assignment 02

	Erick Fernandez de Arteaga - https://www.linkedin.com/in/erickfda
	Version 1.0.0
 ========================================================================================*/

/*========================================================================================
	TABLE OF CONTENTS

	## Player Controls
 ========================================================================================*/

/*========================================================================================
	## Player Controls
========================================================================================*/
One-finger drag to move player.
One-finger double tap to reset player to start.
Two-finger double tap to show/hide map.

“Toggle Day/Night” button toggles the ambient lighting.
“Toggle Fog” button toggles the fog effect.
“Toggle Flashlight” button toggles the use of the flashlight.

Game enthusiasts proclaim this game offers “as much as 30 seconds of entertainment”!